
import './index.scss'
import React,{Component} from 'react'

class DetailTabpanel extends Component {
    render() {
        let detail = this.props.detail.result
        let {list} = detail.detailInfo.detailImage["0"]
        // console.log(detail,2222)
        // console.log(list,11)
        return(
            <div className="detail-tabpanel">
                <div className="tabpanel-top">
                    <nav className="tabpanel-tabs">
                        <li className="tab-item active">图文详情</li>
                        <li className="tab-item">商品参数</li>
                        <li className="tab-item">评价</li>
                        <li className="tab-item">热卖推荐</li>
                    </nav>

                </div>
                <div className="tabpanel-panels">
                    <div className="panels-item">
                        <div className="desc">
                            <p>{detail.itemInfo.desc}</p>
                        </div>
                        <div className="pics">
                            <span>{detail.detailInfo.detailImage["0"].key}</span>
                            <div className="pic-list">
                                {
                                    list.map((item,i) => {
                                        return <img src={item} alt="" key={i}/>
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default DetailTabpanel