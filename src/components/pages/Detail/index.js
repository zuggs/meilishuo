
import './index.scss'
import Primary from './Primary'
import DetailFooter from './DetailFooter'
import DetailTabpanel from './DetailTabpanel'
import axios from 'axios'
import React,{Component} from 'react'

class Detail extends Component {
    constructor(props){
        super(props)

        this.state = {
            detail: null,
            
        }
        
    }

    geiDetail(){
        let iid = this.props.match.params.iid
        // this.setState({iid})
        // console.log(this.state.iid,111)
        axios.get('/dt/detail/mls/v1/h5?iid='+iid+'&_ajax=1&cparam=')
        .then(res=>{
           this.setState({detail: res.data})
        })
    }

    componentWillMount(){
        this.geiDetail()
    }

    renderContent(){
       
        if(!this.state.detail) return ""
        let { detail } = this.state
        // console.log(detail)
        
        return (
            <div className="views">
                <div className="detailPrimary">
                    <div className="detailBanner">
                        <Primary detail={detail}/>
                    </div>
                    <DetailTabpanel detail={detail}/>
                </div>
            </div>
        )
    }
    render() {
        
        return(
            <div className="goodDetail">
                {
                    this.renderContent()
                }
                <DetailFooter/>
            </div>
        )
    }
}

export default Detail