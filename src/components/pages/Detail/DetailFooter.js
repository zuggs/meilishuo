
import './index.scss'
import React,{Component} from 'react'

class DetailFooter extends Component {
    render() {
        return(
            <footer className="detailFooter">
                <div className="foot-L">
                    <div className="foot-im">
                        <i></i>
                        <span>客服</span>
                    </div>
                    <div className="foot-shop">
                        <i></i>
                        <span>店铺</span>
                    </div>
                    <div className="foot-fav">
                        <i></i>
                        <span>收藏</span>
                    </div>
                </div>
                <div className="foot-buy">
                    <span className="buy-cart">加入购物车</span>
                    <span className="buy-now">购买</span>
                </div>
            </footer>
                
            
        )
    }
}

export default DetailFooter