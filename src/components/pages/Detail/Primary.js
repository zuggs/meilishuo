
import './index.scss'
import React,{Component} from 'react'
import Swiper from 'swiper'

const SwiperSlide = props => {
    let {src} = props
    return(
        <div className="swiper-slide">
            <div className="img-box">
                <img width="100%" src={src} alt=""/>
            </div>
        </div>
    )
}

const Info = props => {
    let { goodInfo } = props
    // console.log(props,111)
    return (
        <div className="primaryInfo">
            <div className="info-title">
                <p>{goodInfo.itemInfo.title}</p>
            </div>
            <div className="info-price">
                <span className="now">￥{goodInfo.itemInfo.highNowPrice}</span>
                <span className="market">￥{goodInfo.itemInfo.highPrice}</span>
                <span className="discount">{goodInfo.itemInfo.discountDesc}</span>
            </div>
            <div className="info-other">
                <span className="item">{goodInfo.columns["0"]}</span>
                <span className="item">{goodInfo.columns[1]}</span>
                <span className="item">{goodInfo.shopInfo.services[3].name}</span>
                <span className="item">{goodInfo.itemInfo.extra.sendAddress}</span>
            </div>
            <div className="services">
                <div className="services-info">
                    <span className="services-item">
                        <img src={goodInfo.shopInfo.services["0"].icon} alt=""/>
                        <span>{goodInfo.shopInfo.services["0"].name}</span>
                    </span>
                    <span className="services-item">
                        <img src={goodInfo.shopInfo.services["1"].icon} alt=""/>
                        <span>{goodInfo.shopInfo.services["1"].name}</span>
                    </span>
                    <span className="services-item">
                        <img src={goodInfo.shopInfo.services["2"].icon} alt=""/>
                        <span>{goodInfo.shopInfo.services["2"].name}</span>
                    </span>
                    <i className="iconfont icon-youjiantou"></i>
                </div>
                
            </div>
        </div>
    )
}

class Primary extends Component {
    
    render() {
        // console.log(this.props.detail.result.itemInfo.topImages)
        let { topImages } = this.props.detail.result.itemInfo
        let goodInfo = this.props.detail.result
        return(
            <div className="detailPrimary">
                <div className="swiper-container detail-banner"> 
                    <div className="swiper-wrapper">
                        {
                            topImages.map((item, i) => {
                                return <SwiperSlide key={i} src={item}/>
                            })
                        }
                        
                    </div>
                    <div className="swiper-pagination"></div>
                </div>
                <Info goodInfo={goodInfo}/>
            </div>
        )
    }
    componentDidMount () {
		new Swiper ('.detail-banner',{
			pagination: {
                el: '.swiper-pagination',
                type: 'fraction',
                offsetPxafter: 300
			}
		})
	}
}

export default Primary